# **Conectando a DragonBoard 410C no IBM Watson IoT Platform**


Este tutorial apresenta a instalação do MQTT - Client na DragonBoard 410C para fazer o envio de dados na plataforma IBM Watson IoT Plataform.

## **Instalação MQTT-Client na DragonBoard 410C**

MQTT é um protocolo de comunicação para envio de mensagens muito utilizado em *IoT (Internet of Things)*. Ao utilizar qualquer plataforma que ofereça esse serviço, nesse caso, *Internet of Things da IBM Bluemix*, a plataforma funciona como um servidor que recebe as mensagens enviadas pelos clientes e destina-as para outros clientes. Essa troca de informações é realizada através desse protocolo que utiliza uma estrutura de tópicos e é possível determinar como será a interação entre o cliente e as mensagens. 

#### **Primeiro Passo**

Fazer o download dos pacotes paho_mqtt e cJSON. Esses pacotes são necessários para fazer a comunicação com a plataforma *IBM Bluemix* e enviar os dados. Para fazer o download digite o comando abaixo no terminal: 
```sh
cd /home/$USER 
git clone https://github.com/ibm-messaging/iotf-embeddedc.git 
```

Se ocorrer algum erro (Fig.1) ao executar esse comando, certifique- se de que a data e o horário do sistema estão corretos.  

<div align="center">
    <figure>
        <img src="/Tutorial/Fig.1.png">
        <figcaption>Possível erro ao realizar download.</figcaption>
    </figure>
</div>


Se estiverem incorretos, a partir do seguinte comando é possível fazer a alteração da data do sistema: 

```sh
date -Rdate -s "mm/dd/aaa hh:mm" 
```

#### **Segundo Passo**
Após realizar o download, execute os comandos: 

```sh
cd /home/$USER/iotf-embeddedc 
./setup.sh 
```

Esse comando irá descompactar os pacotes instalados no passo anterior. No final, o terminal ficará semelhante a figura abaixo.


<div align="center">
    <figure>
        <img src="/Tutorial/Fig.2.png">
        <figcaption>Fig.2 - Após finalizar a execução do script.</figcaption>
    </figure>
</div>


#### **Terceiro Passo**

Inicie o processo de instalação dos pacotes. Esse comando resultará na compilação dos exemplos. 

```sh
cd /home/$USER/iotf-embeddedc/samples  
cmake CMakelists.txt 
```

<div align="center">
    <figure>
        <img src="/Tutorial/Fig.3.png">
        <figcaption>Fig.3 - Instalação dos pacotes.</figcaption>
    </figure>
</div>


### **Criando Device na IBM Watson IoT Plataform**
Para utilizar a plataforma é necessário criar uma conta *IBM* e criar o *device* que irá incluir o dispositivo usado para enviar de dados. Se for necessário, é possível adicionar mais dispositivos. 

Link para criar a conta IBM: https://www.ibm.com/cloud-computing/bluemix/pt  

Ao entrar na *IBM Cloud*, no menu *Catalog*, selecione a categoria *Internet of Things* e crie o seu primeiro serviço. Para adicionar dispositivos siga os passos abaixo.

#### **Primeiro Passo**

Selecionar o tipo de dispositivo e nomear com o ID que desejar.

<div align="center">
    <figure>
        <img src="/Tutorial/Fig.4.png">
        <figcaption>Fig.4 - Tipo de dispositivo.</figcaption>
    </figure>
</div>


#### **Segundo Passo**

Inserir as informações do dispositivo incluído no passo anterior. Esse passo é opcional e ajuda na identificação do dispositivo.

<div align="center">
    <figure>
        <img src="/Tutorial/Fig.5.png">
        <figcaption>Descrição do dispositivo.</figcaption>
    </figure>
</div>


#### **Terceiro Passo**
Inserir um token para validação do dispositivo. 

### **Configurações e envio de dados para plataforma**

Ainda no diretório ‘samples’, execute os seguintes comandos para alterar as configurações com as credenciais obtidas no passo anterior.

```sh
cd /home/$USER/iotf-embeddedc/samples  
nano device.cfg 
```

As configurações a serem alteradas são:

```
org = <ID da organização> 
type = SmartHydrometer 
id = sh2 
auth-method = token 
auth-token = <token definido> 
```

Cabe ressaltar que o ID da organização é definido nas configurações da conta criada. 


Após realizar essa alteração nas configurações, execute o comando para começar a enviar os dados na plataforma. 

```sh
./sampleDevice
```

Esses dados podem ser visualizados de forma gráfica. Para isso, basta criar **placas** e adicionar **cartões** referentes aos dados que serão apresentados. Essas **placas** e **cartões** podem ser encontrados ao acessar seu **_dashboard_** no menu **_Placas_**.

<div align="center">
    <figure>
        <img src="/Tutorial/dashboard.png">
        <figcaption>Dashboard.</figcaption>
    </figure>
</div>


#### **Bibliotecas em Python**

Para utilizar esses recursos em python basta instalar o módulo **_ibmiotf_**.

```sh
pip install ibmiotf
```

Um exemplo de aplicação é mostrado abaixo.

```python
import time 
import sys 
import uuid 
import argparse 
import ibmiotf.device 

def myOnPublishCallback(): 
    print("Confirmed event %s received by IoTF\n" % x) 

def commandProcessor(cmd): 
    print("Command received: %s" % cmd.data) 

def sendData(cfg, evt, data): 
    deviceOptions = ibmiotf.device.ParseConfigFile(cfg) 
    deviceCli = ibmiotf.device.Client(deviceOptions) 
    deviceCli.commandCallback = commandProcessor 

    # Connect and send datapoint(s) into the cloud 
    deviceCli.connect() 

    data = { 'Value' : data} 
    success = deviceCli.publishEvent(evt, "json", data, qos=0, on_publish=myOnPublishCallback) 

    if not success: 
        print("Not connected to IoTF") 

    # Disconnect the device and application from the cloud 
    deviceCli.disconnect() 
    
    
authMethod = None 

# Initialize the properties we need 
parser = argparse.ArgumentParser() 

# Primary Options 
parser.add_argument('-c', '--cfg', required=True, default=None, help='configuration file') 
parser.add_argument('-d', '--data', required=True, default=None, help='data to send') 
parser.add_argument('-e', '--event', required=True, default=None, help='dashboard event') 

args, unknown = parser.parse_known_args() 

if (args.cfg is None) or (args.data is None): 
    print("cmd error!") 
else: 
   sendData(args.cfg, args.event, int(args.data)) 
```
Para executar o script:

```sh
git clone https://gitlab.com/qualcomm-iot-reference-center/watson_iot.git
cd watson_iot
python ibmiotdevice.py -c device.cfg -e "ValueChanged" -d 10 
```

>Os parâmetros são: 
-c: arquivo de configuração; 
-e: evento relacionado com o data que será publicado; 
-d: informação que será enviada. 

O resultado de execução pode ser monitorado no relatório de eventos do dispositivo.

![](/Tutorial/eventos.png)

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>

