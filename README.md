# Connecting the DragonBoard 410C on the IBM Watson IoT Platform


This tutorial introduces the MQTT - Client installation on the DragonBoard 410C to send data to the IBM Watson IoT Platform.

## Installation MQTT-Client on DragonBoard 410C

MQTT is a widely used communication protocol for sending messages in *IoT (Internet of Things)*. When using any platform that offers this service, in this case, *Internet of Things of IBM Bluemix*, the platform acts as a server that receives the messages sent by the clients and destines them to other clients. This information exchange is performed through this protocol that uses an outline, and it is possible to determine how the interaction between the client and the messages will be.

#### First step

Install cmoka:

```sh
cd /home/$USER 
curl -O https://git.cryptomilk.org/projects/cmocka.git/snapshot/cmocka-1.1.0.zip
unzip cmocka-1.1.0.zip
rm -f cmocka-1.1.0.zip
cd cmocka-1.1.0/
mkdir build && cd build/
cmake ..
sudo make install
```


#### Second step

Download the paho_mqtt and cJSON packages. These packages are required to communicate with the *IBM Bluemix* platform and send the data. To download, type the command below into the terminal:

```sh
cd /home/$USER 
git clone https://github.com/ibm-messaging/iotf-embeddedc.git 
```

If an error occurs (Fig.1) when executing this command, make sure that the system date and time are correct.

<div align="center">
    <figure>
        <img src="/Tutorial/Fig.1.png">
        <figcaption>Possible error while downloading.</figcaption>
    </figure>
</div>

If they are incorrect, from the following command it is possible to change the date of the system:

```sh
date -Rdate -s "mm/dd/aaa hh:mm" 
```

After downloading, run the commands:

```sh
cd /home/$USER/iotf-embeddedc 
./setup.sh 
```

This command unpacks the packages installed in the previous step. In the end, the terminal will look similar to the figure below.

<div align="center">
    <figure>
        <img src="/Tutorial/Fig.2.png">
        <figcaption>Fig.2 - Console output after you finish running the script.</figcaption>
    </figure>
</div>

#### Third step

Start the package installation process and compile the samples.

```sh
mkdir build && cd build
cmake ..
make 
export IOT_EMBDC_HOME=$HOME/iotf-embeddedc/
```

After the *cmake* command, some files and folders are created inside the build directory. By executing *make*, the samples are compiled and written to the build/samples folder.

Now, create a device on the platform, as described in the guide below.

## **Creating Device on IBM Watson IoT Platform**

To use the platform you need to create an *IBM* account and create the *device categoty* that includes the device used to send data. If necessary, you can add more devices.

> Link to create the IBM account: https://www.ibm.com/cloud-computing/bluemix

When entering the IBM Cloud in the Catalog menu, select the *Internet of Things* category and create your first service. To add devices follow the steps below.

#### First step

Select the type of device and name it with the ID you want.

<div align="center">
    <figure>
        <img src="/Tutorial/ID.png">
        <figcaption>Fig.3 - Device category.</figcaption>
    </figure>
</div>

#### Second step

Enter the device information included in the previous step. This step is optional and helps in identifying the device.

<div align="center">
    <figure>
        <img src="/Tutorial/Desc.png">
        <figcaption>Fig.4 - Device description.</figcaption>
    </figure>
</div>

#### Third step

Enter a token for device validation.

<div align="center">
    <figure>
        <img src="/Tutorial/Token.png">
        <figcaption>Fig.5 - Token.</figcaption>
    </figure>
</div>


### **Configurations and sending data to platform**

Back in the Dragonboard, change the device configuration file, with the data generated in the creation of the device.

```sh
cd $IOT_EMBDC_HOME/samples 
nano device.cfg 
```

The settings to be changed are:

```
org=<your org ID>
domain=
type=<the type you created>
id=<ID entered in the creation process>
auth-method=token
auth-token=<token generated or informed at creation>
serverCertPath=
useClientCertificates=0
rootCACertPath=
clientCertPath=
clientKeyPath= 
```

> It should be noted that the organization ID is defined in the created account settings (Menu -> Settings -> identity), and can also be found in the upper right corner of the platform screen, below your username.


Save the change to the settings, and you can run the test scripts to begin sending the data to the platform. The code should start transmission, and data can be viewed on the device page on the IBM platform.


```sh
cd $IOT_EMBDC_HOME/build/samples
./sampleDevice
```

This data can be viewed graphically. To do this, create **cards** and add **cards** referring to the data that will be presented. These **boards** and **cards** can be found by accessing your ** _dashboard_** in the menu ** _boards_**.

<div align="center">
    <figure>
        <img src="/Tutorial/dashboard.png">
        <figcaption>Fig.6 - Dashboard.</figcaption>
    </figure>
</div>


## Libraries in Python

To use these features in python install the module **_ibmiotf_**.

```sh
pip install ibmiotf
```

An example application is shown below.

```python
import time 
import sys 
import uuid 
import argparse 
import ibmiotf.device 

def myOnPublishCallback(): 
    print("Confirmed event %s received by IoTF\n" % x) 

def commandProcessor(cmd): 
    print("Command received: %s" % cmd.data) 

def sendData(cfg, evt, data): 
    deviceOptions = ibmiotf.device.ParseConfigFile(cfg) 
    deviceCli = ibmiotf.device.Client(deviceOptions) 
    deviceCli.commandCallback = commandProcessor 

    # Connect and send datapoint(s) into the cloud 
    deviceCli.connect() 

    data = { 'Value' : data} 
    success = deviceCli.publishEvent(evt, "json", data, qos=0, on_publish=myOnPublishCallback) 

    if not success: 
        print("Not connected to IoTF") 

    # Disconnect the device and application from the cloud 
    deviceCli.disconnect() 
    
    
authMethod = None 

# Initialize the properties we need 
parser = argparse.ArgumentParser() 

# Primary Options 
parser.add_argument('-c', '--cfg', required=True, default=None, help='configuration file') 
parser.add_argument('-d', '--data', required=True, default=None, help='data to send') 
parser.add_argument('-e', '--event', required=True, default=None, help='dashboard event') 

args, unknown = parser.parse_known_args() 

if (args.cfg is None) or (args.data is None): 
    print("cmd error!") 
else: 
   sendData(args.cfg, args.event, int(args.data)) 
```


To run the script:

```sh
git clone https://gitlab.com/qualcomm-iot-reference-center/watson_iot.git
cd watson_iot
nano device.cfg
```

Change the contents of the configuration file to the following format, replacing the locations indicated by <> for your IBM account information:


```
[device]
org=<>
type=<>
id=<>
auth-method=token
auth-token=<>
```

Save the changes and run the python script:

```sh
python ibmiotdevice.py -c device.cfg -e "ValueChanged" -d 10
```

> The parameters are:
-c: configuration file;
-e: event related to the data that will be published;
-d: information to be sent.


The execution result can be monitored in the device event report.

![](/Tutorial/eventos.png)